# rpmbuild

#### 介绍
rpmbuild rpm 包构建工具

#### 软件架构
软件架构说明：
-     BUILD     编译时所用的暂存目录
-     RPMS      放打包好的二进制rpm包
-     SOURCES   放置源代码和补丁文件
-     SPECS     放置spec文件
-     SRPMS     放置RPM源码包


#### 安装教程
```
1.  安装rpm打包所需的工具
sudo yum -y install rpmdevtools
2.  生成rpm打包目录，默认生成在 root 路径下
rpmdev-setuptree
3.  进入SPECS目录，生成spec文件模板
cd rpmbuild/SPECS
rpmdev-newspec  test.spec

error: 错误：空 %file 文件
rpmbuild -bb SPECS/pure-ftpd.spec --nodebuginfo
```


#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
